library=
name=NSS Internal PKCS #11 Module
parameters=configdir='sql:/home/ale/.thunderbird/ev4xmct7.default' certPrefix='' keyPrefix='' secmod='secmod.db' flags=optimizeSpace updatedir='' updateCertPrefix='' updateKeyPrefix='' updateid='' updateTokenDescription=''  manufacturerID='Mozilla.org' libraryDescription='Serviços cript. internos do PSM' cryptoTokenDescription='Serviços criptográficos gerais' dbTokenDescription='Disp. de segurança em software' cryptoSlotDescription='Serviços criptográficos internos do PSM' dbSlotDescription='Chaves privada do PSM' FIPSSlotDescription='Serviços de criptografia, chave e certificados FIPS 140' FIPSTokenDescription='Disp. de segurança em SW (FIPS)' minPS=0
NSS=Flags=internal,critical trustOrder=75 cipherOrder=100 slotParams=(1={slotFlags=[ECC,RSA,DSA,DH,RC2,RC4,DES,RANDOM,SHA1,MD5,MD2,SSL,TLS,AES,Camellia,SEED,SHA256,SHA512] askpw=any timeout=30})

