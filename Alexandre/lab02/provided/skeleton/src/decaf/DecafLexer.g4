lexer grammar DecafLexer;

@header {
package decaf;
}

options
{
  language=Java;
}

TK_class : 'class Program';

LCURLY : '{';
RCURLY : '}';

BOOLEAN: 'boolean';

BREAK: 'break';

CALLOUT: 'callout';

CLASS: 'class';

CONTINUE: 'continue';

ELSE: 'else';

FALSE: 'false';

FOR: 'for';

INT: 'int';

IF: 'if';

RETURN: 'return';

TRUE: 'true';

VOID: 'void';

MENOS: '-';

INTLITERAL:

ID  :
  ('a'..'z' |'A'..'Z'|'0'..'9'|'_')+;

WS_ : (' ' | '\n' ) -> skip;

SL_COMMENT : '//' (~'\n')* '\n' -> skip;

CHARLITERAL : '\'' (ESC|CHAR) '\'';

STRING : '"' (ESC|CHAR)* '"';

fragment ESC :  '\\' ('n'|'"'|'t'|'\\'|'\'');

fragment CHAR: (' '|'!'|'#'|'$'|'%'|'&'|'('|')'|'*'|'+'|','|'-'|'.'|'/'|'0'..'9'|':'|';'|'<'|'='|'>'|'?'|'@'|'A'..'Z'|'['|']'|'^'|'_'|'`'|'a'..'z'|'{'|'|'|'}'|'~');




